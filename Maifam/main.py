#!/usr/bin/env python3
# from pytg.receiver import Receiver  # get messages
from pytg.sender import Sender  # send messages, and other querys.
# from pytg.utils import coroutine
# from pytg import Telegram
import pytg


def send(message, delay):
    sender.send_msg("@KampungMaifamBot", message)
    print(message)
    pytg.sleep(delay)


# receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)


############
# SETTINGS #
############

settings = {
        # general settings
        "delay":            8,  # delay in seconds between each command
        "wait_min":         5,  # how much minutes wait after each round

        # on/off
        "eating":           True,
        "animal":           True,
        "mining":           True,
        "farm":             True,
        "fishing":          True,

        # timings settings
        "eating_time":      90,  # eat every x mins
        "animal_time":      15,  # harvest animals every x mins
        "mining_time":      1,  # one mining every x mins

        # mining settings
        "mining_amount":    1,

        # farm settings
        "farm_spaces":      80,
        "farm_item":        "Strawberry",
        "farm_sell_items":  False,
        "farm_buy_items":   False,

        # fishing settings
        "fishing_river":    "Soprano Lake"
    }

fishing_rivers = {
        "Lala River": {
            "time": 2
        },
        "Mimi River": {
            "time": 3
        },
        "Badabu River": {
            "time": 5
        },
        "Prisoner's Lake": {
            "time": 1
        },
        "Soprano Lake": {
            "time": 5
        }}

farm_items = {
        "Strawberry": {
            "spaces": 1,
            "time": 5,
            "plant": True,
            "gewinn": 0
        },
        "Potato": {
            "spaces": 1,
            "time": 4,
            "plant": True,
            "gewinn": 110
        },
        "Cucumber": {
            "spaces": 2,
            "time": 10,
            "plant": True,
            "gewinn": 700
        },
        "Carrot": {
            "spaces": 1,
            "time": 3,
            "plant": True,
            "gewinn": 220
        },
        "Tomato": {
            "spaces": 2,
            "time": 12,
            "plant": True,
            "gewinn": 400
        },
        "RedPepper": {
            "spaces": 2,
            "time": 12,
            "plant": True,
            "gewinn": 0
        },
        "Banana": {
            "spaces": 5,
            "time": 50,
            "plant": True,
            "gewinn": 5000
        },
        "Orange": {
            "spaces": 5,
            "time": 30,
            "plant": False,
            "gewinn": 1500
        },
        "Apple": {
            "spaces": 5,
            "time": 30,
            "plant": False,
            "gewinn": 2200
        }}

# wool: 15000 / 90min = 166per min / 3 = .. per space per min
# milk: 3000  / 12min = 250per min / 5 = 50 per space per min
# egg:  500   / 15min = 33.33per min 
# goatyMilk: 1000 / 20min = ..per min / 2 = .. per space per min

# cheese: 6000


def main():
    # Settings
    delay = settings["delay"]
    wait_min = settings["wait_min"]
    # eating
    eating = settings["eating"]
    eating_time = settings["eating_time"]
    # animal
    animal = settings["animal"]
    animal_time = settings["animal_time"]
    # mining
    mining = settings["mining"]
    mining_time = settings["mining_time"]
    mining_amount = settings["mining_amount"]
    # farm
    farm = settings["farm"]
    farm_spaces = settings["farm_spaces"]
    farm_item = settings["farm_item"]
    farm_sell_items = settings["farm_sell_items"]
    farm_buy_items = settings["farm_buy_items"]
    # fishing
    fishing = settings["fishing"]
    fishing_river = settings["fishing_river"]
    # Variablen Initialisierung
    counter = 0
    farm_counter = 0
    animal_counter = 0
    eating_counter = 0
    fishing_counter = 0
    mining_counter = 0
    farm_ready = False
    animal_ready = False
    eating_ready = False
    mining_ready = False
    currently_fishing = False  # ignore all other commands while fishing
    while True:
        print(counter)
        something_happened = False
        # Fishing, has to be the first command
        if fishing:
            # 15 min fishing duration
            fishing_time = fishing_rivers[fishing_river]["time"]
            time_over = fishing_counter % int(fishing_time) == 0
            time_over = fishing_counter >= fishing_time
            if time_over:
                send("Pull The Fishing Rod", delay)
                currently_fishing = False

        # mining
        time_over = mining_counter % int(mining_time) == 0
        time_over = mining_counter >= mining_time
        if mining:
            if not currently_fishing:
                if time_over or mining_ready:
                    for i in range(mining_amount):
                        send("⛏", delay)
                        print("Mining: ⛏")
                    something_happened = True
                    mining_ready = False
                    mining_counter = 0
            elif time_over:
                mining_ready = True

        # eat every hour
        time_over = eating_counter % int(eating_time) == 0
        time_over = eating_counter >= eating_time
        if eating:
            if not currently_fishing:
                if time_over or eating_ready:
                    send("/eat_RegularSalad", delay)
                    something_happened = True
                    eating_ready = False
                    eating_counter = 0
            elif time_over:
                eating_ready = True

        # Animals
        time_over = animal_counter % int(animal_time) == 0
        time_over = animal_counter >= animal_time
        if animal:
            if not currently_fishing:
                if time_over or animal_ready:
                    send("/harvestAnimal", delay)  # 5 energy
                    send("/feed", delay)           # 5 energy
                    something_happened = True
                    animal_ready = False
                    animal_counter = 0
            elif time_over:
                animal_ready = True

        # Farm
        farm_item_time = farm_items[farm_item]["time"]
        farm_item_spaces = farm_items[farm_item]["spaces"]
        farm_item_plant = farm_items[farm_item]["plant"]
        farm_item_amount = int(farm_spaces/farm_item_spaces)
        time_over = farm_counter % int(farm_item_time) == 0
        time_over = farm_counter >= farm_item_time
        if farm:
            if not currently_fishing:
                if time_over or farm_ready:
                    send("/harvestCrops", delay)
                    # /sell last_farm_item, wenn ich das abwechseln will
                    if farm_sell_items:
                        cmd = "/sell_"+str(farm_item)+"_"+str(farm_item_amount)
                        send(cmd, delay)
                    # wenn sie gekauft werden sollten, und gepflanzt werden
                    # wenn sie nicht gepflanzt werden, sollen auch keine
                    # gekauft werden
                    if farm_buy_items and farm_item_plant:
                        cmd = "/buy_"+str(farm_item)+"_"+str(farm_item_amount)
                        send(cmd, delay)
                    if farm_item_plant:
                        fia = farm_item_amount
                        cmd = "/plant_"+str(farm_item)+"_"+str(fia)
                        send(cmd, delay)
                    send("/water", delay)
                    something_happened = True
                    farm_ready = False
                    farm_counter = 0
            elif time_over:
                farm_ready = True

        # the last command for fishing
        if fishing:
            if not currently_fishing:
                send(fishing_river, 0)
                currently_fishing = True
                fishing_counter = 0
        elif something_happened:
            send("/sleep", 0)

        # sleep 3 mins
        counter += wait_min
        farm_counter += wait_min
        animal_counter += wait_min
        eating_counter += wait_min
        fishing_counter += wait_min
        mining_counter += wait_min
        pytg.sleep(wait_min*60)


if __name__ == "__main__":
    main()
