#!/usr/bin/env python3
from pytg.receiver import Receiver  # get messages
from pytg.sender import Sender  # send messages, and other querys.
import pytg

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)
delay = 4
count = 0

while True:
    sender.send_msg("@KampungMaifamBot", "⛏")
    print("Mining "+str(count)+": ⛏")
    pytg.sleep(5)
    count += 1
