#!/usr/bin/env python3
from pytg.sender import Sender
from pytg.receiver import Receiver
import time
import sys

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)

file = "pokemon_sorted.txt"
command = "inspect"
offset = 0

# Commandline args
if len(sys.argv) >= 2:
    command = sys.argv[1]
if len(sys.argv) >= 3:
    offset = int(sys.argv[2])

cmds = open(file).read().split("\n")
for i in range(offset, cmds.__len__()-1):
    if("#" in cmds[i][0:1]):
        continue
    poke_name = cmds[i].split(";")[1]
    cmd = "/"+command+" "+poke_name
    print(str(i+1)+"/"+str(cmds.__len__()-1))
    # print(str(cmd))
    sender.send_msg("@newHeXabot", str(cmd))
    time.sleep(0.5)
