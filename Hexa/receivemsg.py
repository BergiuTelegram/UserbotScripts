"""
A simple bot reacting to messages.
like the dump bot, but it responds to every `ping` with a `pong`.
"""
from __future__ import unicode_literals
from pytg.receiver import Receiver  # get messages
from pytg.sender import Sender  # send messages, and other querys.
from pytg.utils import coroutine
import re
import time

__author__ = 'luckydonald'

FWD_PEER = "@GeloMyrtol"
MY_POKEMON_FILE = "my_pokemon.txt"

def read_pokes_from_file(file):
    pokes = open(file).read()
    tmp = []
    for poke in pokes.split("\n"):
        _tuple = poke.split(";")
        if len(_tuple) >= 2:
            tmp.append(_tuple[1])
    return tmp

def main():
    receiver = Receiver(host="localhost", port=4458)
    sender = Sender(host="localhost", port=4458)
    receiver.start()  # note that the Sender has no need for a start function.
    # initialize status
    status = {}
    status['pokemon'] = read_pokes_from_file(MY_POKEMON_FILE)
    receiver.message(example_function(sender, status))
    receiver.stop()

def get_text(msg):
    """
    Returns the text of a message
     @msg: a message object
    """
    text = None
    if 'text' in msg:
        text = msg.text
    elif 'media' in msg and 'caption' in msg.media:
        text = msg.media.caption
    return text

def collecting(sender, msg, text, status, reg):
    # MEGASTONES
    # encountered megastone
    if re.match(reg['enc_megastone'], text):
        sender.reply(msg.id, "/collect")
        return True
    # collected megastone
    if re.match(reg['col_megastone'], text):
        sender.fwd("@GeloMyrtol", msg.id)
        sender.reply(msg.id, "/hunt")
        return True
    return False

def hunting(sender, msg, text, status, reg):
    # HUNTING
    # encountered wild pokemon
    m = re.search(reg['enc_pokemon'], text)
    if m:
        poke_name = m.group(1)
        poke_chance = int(m.group(2))
        if poke_name not in status['pokemon'] and poke_chance <= 1:
            status['last_pokemon_msg'] = msg
            sender.reply(msg.id, "/catch")
        else:
            sender.send_msg(msg.sender.cmd, "/hunt")
        return True
    # ball failed
    if re.match(reg['ball_failed'], text):
        sender.send_msg(msg.sender.cmd, "/hunt")
        sender.fwd("@GeloMyrtol", msg.id)
        return True
    # already have pokemon
    if re.match(reg['already_have_pokemon'], text):
        if status['last_pokemon_msg'] != None:
            last_text = get_text(status['last_pokemon_msg'])
            m = re.search(reg['enc_pokemon'], last_text)
            if m:
                poke_name = m.group(1)
                status["pokemon"].append(poke_name)
        sender.send_msg(msg.sender.cmd, "/hunt")
        sender.fwd("@GeloMyrtol", msg.id)
        return True
    # caught pokemon
    m = re.match(reg['caught_pokemon'], text)
    if m:
        status["pokemon"].append(m.group(1))
        sender.send_msg(msg.sender.cmd, "/hunt")
        sender.fwd("@GeloMyrtol", msg.id)
        return True
    return False

def buying(sender, msg, text, status, reg):
    # BUYING BALLS
    # no balls left
    if re.match(reg['no_balls'], text):
        sender.send_msg(msg.sender.cmd, "/buy regular 10")
        return True
    # puchased_balls
    if re.match(reg['purchased_balls'], text):
        sender.send_msg(msg.sender.cmd, "/hunt")
        return True
    # broke, stop playing :(
    if re.match(reg['broke'], text):
        return True
    return False

@coroutine
def example_function(sender, status):
    try:
        # All Regexes
        reg = {}
        reg['enc_megastone'] = ".*You encountered a .* mega stone.*"
        reg['col_megastone'] = ".*You collected a .* mega stone.*"
        reg['enc_pokemon'] = ".*You encountered a wild (.*)\n([0-9]+)\% catch probability.*"
        reg['ball_failed'] = ".*Your .* ball failed.*"
        reg['already_have_pokemon'] = "You already have this pokemon"
        reg['caught_pokemon'] = ".*You caught a wild (.*)"
        reg['no_balls'] = ".*You have no more .* balls.*"
        reg['purchased_balls'] = ".*You purchased .* balls.*"
        reg['broke'] = ".*Not enough Poke Dollars.*"
        # safe last message from hunt
        status['last_pokemon_msg'] = None;
        quit = False
        while not quit:
            msg = (yield)
            sender.status_online()
            if msg.event != "message":
                continue
            if msg.own:
                # message is from me
                continue
            text = get_text(msg)
            if text is None:
                # komisch
                continue
            # rest ist nur für hexa bot
            if 'username' not in msg.peer or msg.peer.username != "newHeXabot":
                continue
            time.sleep(1)
            if collecting(sender, msg, text, status, reg) or\
                    hunting(sender, msg, text, status, reg) or\
                    buying(sender, msg, text, status, reg):
                continue
    except GeneratorExit:
        # the generator (pytg) exited (got a KeyboardIterrupt).
        pass
    except KeyboardInterrupt:
        # we got a KeyboardIterrupt(Ctrl+C)
        pass
    else:
        # the loop exited without exception, becaues _quit was set True
        pass


# # program starts here ##
if __name__ == '__main__':
    main()  # executing main function.
    # Last command of file (so everything needed is already loaded above)
