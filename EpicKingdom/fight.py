#!/bin/python3
from pytg.sender import Sender
from pytg.receiver import Receiver
import time
import sys

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)

# Commandline args
if len(sys.argv) >= 2:
    msg = sys.argv[1]
else:
    msg = "/W_I9C21618"

enmys = open("enemies").read().split("\n")
x = 0
for enmy in enmys:
    knights = int(enmy.split(";")[0])
    amd = str(knights/15+2)
    i = 0
    while i < 3:
        print(i, enmy.split(";")[1])
        sender.send_msg("@epickingbot", enmy.split(";")[1])
        time.sleep(2)
        sender.send_msg("@epickingbot", amd)
        time.sleep(2)
        i += 1
        x += int(knights/15+2)+1
    if x >= 27:
        minuten = 16
        sekunden = minuten*60
        schritte = 30
        while sekunden > 0:
            print(str(sekunden/60)+" min remainig till next fight")
            time.sleep(schritte)
            sekunden -= schritte
        x = 0
