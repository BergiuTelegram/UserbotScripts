#!/bin/python3
from pytg.sender import Sender
from pytg.receiver import Receiver
import time
import sys

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)

# Commandline args
if len(sys.argv) >= 2:
    msg = sys.argv[1]
else:
    msg = "/build_4_4"

i = 0
while True:
    sender.send_msg("@epickingbot", msg)
    print(i, msg)
    i += 1
    time.sleep(2)
