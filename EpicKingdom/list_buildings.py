#!/bin/python3
from pytg.sender import Sender
import sys
import time

sender = Sender(host="localhost", port=4458)

# Commandline args
if len(sys.argv) >= 2:
    s = sys.argv[1]
else:
    s = "store"

if len(sys.argv) >= 3:
    i = int(sys.argv[2])
else:
    i = 1

if len(sys.argv) >= 4:
    end = int(sys.argv[3])
else:
    end = 125

while i < end:
    msg = "/upgrade_"+s+" 👉 "+str(i)
    sender.send_msg("@epickingbot", msg)
    print(i, msg)
    i += 1
    time.sleep(2)
