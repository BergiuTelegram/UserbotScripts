#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from pytg.sender import Sender
from pytg.receiver import Receiver
import time
import sys

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)

# Commandline args
if len(sys.argv) >= 2:
    file = sys.argv[1]
else:
    file = "commands.txt"
if len(sys.argv) >= 3:
    offset = int(sys.argv[2])
else:
    offset = 0

cmds = open(file).read().split("\n")
for i in range(offset, cmds.__len__()):
    cmd = cmds[i]
    if cmd.find("/") >= 0:
        print(str(i)+"/"+str(cmds.__len__()))
        sender.send_msg("@epickingbot", str(cmd))
        time.sleep(2)
