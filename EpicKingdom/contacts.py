#!/bin/python3
from pytg.sender import Sender
from pytg.receiver import Receiver

receiver = Receiver(host="localhost", port=4458)
sender = Sender(host="localhost", port=4458)

contacts = sender.contacts_list()

for contact in contacts:
    print(contact)

