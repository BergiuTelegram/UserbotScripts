#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pytg.sender import Sender  # send messages, and other querys.
import pytg

sender = Sender(host="localhost", port=4458)


def send(message, delay):
    sender.send_msg("Werwolf", message)
    print(message)
    pytg.sleep(delay)


while True:
    send("/extend 300", 200)
